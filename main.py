from graph.graph import Graph
from graph.node import Node

node0 = Node("FISI")
node0.set_number(0)

node1 = Node("Meca")
node1.set_number(1)

node2 = Node("Odonto")
node2.set_number(2)

node3 = Node("Psicologia")
node3.set_number(3)

node4 = Node("Educacion")
node4.set_number(4)

node5 = Node("FIEE")
node5.set_number(5)

node6 = Node("Ing. Geografica")
node6.set_number(6)

node7 = Node("Rectorado")
node7.set_number(7)

node8 = Node("Biblioteca Central")
node8.set_number(8)

node9 = Node("Ing. civil")
node9.set_number(9)

node10 = Node("Letras")
node10.set_number(10)

my_graph = Graph(17)

#NODO DE INICIO
my_graph.add_node(node0)

#NODOS DESTINO
my_graph.add_node(node1)
my_graph.add_node(node2)
my_graph.add_node(node3)
my_graph.add_node(node4)
my_graph.add_node(node5)
my_graph.add_node(node6)
my_graph.add_node(node7)
my_graph.add_node(node8)
my_graph.add_node(node9)
my_graph.add_node(node10)

#AÑADIENDO ARISTAS
my_graph.add_edge(node0, node1, 200)
my_graph.add_edge(node1, node0, 200)

my_graph.add_edge(node0, node2, 50)
my_graph.add_edge(node2, node0, 50)

my_graph.add_edge(node0, node3, 100)
my_graph.add_edge(node3, node0, 100)

my_graph.add_edge(node0, node4, 150)
my_graph.add_edge(node4, node0, 150)

my_graph.add_edge(node0, node5, 175)
my_graph.add_edge(node5, node0, 175)

my_graph.add_edge(node0, node6, 200)
my_graph.add_edge(node6, node0, 200)

my_graph.add_edge(node0, node7, 300)
my_graph.add_edge(node7, node0, 300)

my_graph.add_edge(node0, node8, 300)
my_graph.add_edge(node8, node0, 300)

my_graph.add_edge(node0, node9, 175)
my_graph.add_edge(node9, node0, 175)

my_graph.add_edge(node1, node9, 50)
my_graph.add_edge(node9, node1, 50)

my_graph.add_edge(node2, node3, 100)
my_graph.add_edge(node3, node2, 175)

my_graph.add_edge(node5, node6, 75)
my_graph.add_edge(node6, node5, 75)

my_graph.add_edge(node7, node8, 50)
my_graph.add_edge(node8, node7, 50)

my_graph.add_edge(node4, node5, 150)
my_graph.add_edge(node5, node4, 150)

my_graph.add_edge(node3, node5, 175)
my_graph.add_edge(node5, node3, 175)

my_graph.add_edge(node2, node4, 150)
my_graph.add_edge(node4, node2, 150)

my_graph.add_edge(node5, node9, 75)
my_graph.add_edge(node9, node5, 75)

my_graph.add_edge(node9, node7, 175)
my_graph.add_edge(node7, node9, 175)

my_graph.add_edge(node6, node5, 75)

my_graph.add_edge(node8, node7, 50)

print("")
print(my_graph.node_exists(node5))
print(my_graph.adjacent(node1, node2))
print(my_graph.adjacent(node2, node3))
print("")
print(my_graph.to_string())
