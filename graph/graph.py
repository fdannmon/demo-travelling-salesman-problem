#! /usr/bin/env python3

from .node import Node
from .edge import Edge

class Graph(object):

    def __init__(self, num_nodes):
        self.__nodes = []
        self.__quantity = num_nodes
        self.__order = 0

    def node_exists(self, node_search):
        if not isinstance(node_search, Node):
            raise TypeError("Argument type is invalid.")
        else:
            return node_search.get_value() in [node.get_value() for node in self.__nodes]

    def edge_exists(self, edge_search):
        if not isinstance(edge_search, Edge):
            raise TypeError("Argument type is invalid")
        else:
            for node in self.__nodes:
                for edge in node.get_edges():
                    if edge.is_equal(edge_search):
                        return True
            return False
        
    def add_node(self, new_node):
        if not isinstance(new_node, Node):
            raise TypeError("Invalid type") 
        else:
            if self.node_exists(new_node):
                print("Nodo ya existe")
                return False
            else:
                self.__nodes.append(new_node)
                return True

    def add_edge(self, origin_node, destiny_node, weight):
        if not isinstance(origin_node, Node) or not isinstance(destiny_node, Node) or not isinstance(weight, int):
            raise TypeError("Some argument is invalid.")
        elif not self.node_exists(origin_node):
            print("Origin node doesn\'t exist.")
            return False
        elif not self.node_exists(destiny_node):
            print("Destiny node doesn\'t exist.")
            return False
        else:
            new_edge = Edge(destiny_node.get_number(), weight)
            for edge in origin_node.get_edges():
                if edge.is_equal(new_edge):
                    print("Edge is already exist")
                    return False
            origin_node.get_edges().append(new_edge)
            print("New edge ({}, {}, {}) has been created.".format(origin_node.get_value(), destiny_node.get_value(), weight))
            return True

    def delete_edge(self, origin_node, destiny_node):
        if not isinstance(origin_node, Node) or not isinstance(destiny_node, Node):
            raise TypeError("Arguments are not valid")
        elif not self.node_exists(origin_node):
            print("Origin node doesn\'t exist.")
            return False
        elif not self.node_exists(destiny_node):
            print("Destiny node doesn\'t exist.")
            return False
        else:
            for edge in origin_node.get_edges():
                if edge.get_destiny() == destiny_node.get_number():
                    origin_node.get_edges().remove(edge)
                    return True
            return False

    def adjacent(self, origin_node, destiny_node):
        if not isinstance(origin_node, Node) or not isinstance(destiny_node, Node):
            raise TypeError("Arguments are not valid")
        elif not self.node_exists(origin_node):
            print("Origin node doesn\'t exist.")
            return False
        elif not self.node_exists(destiny_node):
            print("Destiny node doesn\'t exist.")
            return False
        else:
            return destiny_node.get_number() in [edge.get_destiny() for edge in origin_node.get_edges()]

    def to_string(self):
        this_info = ""
        for one_node in self.__nodes:
            one_info = "{} -> {}\n".format(one_node.info(), one_node.edges_complete_info())
            this_info = this_info + one_info

        return this_info
