#! /usr/bin/env python3

from .edge import Edge

class Node(object):
    
    def __init__(self, value):
        self.__value = value
        self.__number = -1
        self.__edges_list = []

    def get_value(self):
        return self.__value

    def set_number(self, number):
        if not isinstance(number, int):
           raise TypeError("Argument type is invalid")
        else:
            self.__number = number

    def get_number(self):
        return self.__number

    def get_edges(self):
        return self.__edges_list

    def edges_info(self):
        edges_info = []
        for edge in self.__edges_list:
            edges_info.append(edge.info())
        return edges_info

    def edges_complete_info(self):
        edges_compl_info = []
        for edge in self.__edges_list:
            edges_compl_info.append((self.__number, edge.get_destiny(), edge.get_weight()))
        return edges_compl_info

    def is_equal_to(self, other_node):
        if not isinstance(other_node, Node):
            raise TypeError("Argument type is not correct.")
        else:
            return self.__value.lower() == other_node.get_value.lower()

    def info(self):
        return (self.__number, self.__value)

if __name__ == "__main__":
    pass
