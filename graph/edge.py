#! /usr/bin/env python3

class Edge(object):

    def __init__(self, destiny_number, weight):
        self.__destiny = destiny_number
        self.__weight = weight

    def get_destiny(self):
        return self.__destiny

    def get_weight(self):
        return self.__weight
    
    def info(self):
        return (self.__destiny, self.__weight)

    def is_equal(self, other_edge):
        if not isinstance(other_edge, Edge):
            raise TypeError("Type  is not correct.")
        else:
            return self.__destiny == other_edge.get_destiny()


if __name__ == "__main__":
    pass
